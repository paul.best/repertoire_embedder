import pandas as pd
import os
import argparse

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, \
    description="""This script fetches the annotations stored via the sorting of .png files.
                For each sample (.png file), the name of its parent folder will be set as label in the detection.pkl file (column type).
                For instance, create a folder named annotation_toto. In it, create a folder Phee containing .png spectrograms of Phee vocalisations,
                and another folder named Trill etc...
                In these folders, add samples of the correct type to store annotations.""")
parser.add_argument('annot_folder', type=str, help='Name of the folder containing annotations.')
parser.add_argument("detections", type=str, help=".csv file with detections that were clustered (labels will be added to it)")
args = parser.parse_args()

df = pd.read_csv(args.detections)

for label in os.listdir(args.annot_folder+'/'):
    for file in os.listdir(f'{args.annot_folder}/{label}/'):
        df.loc[int(file.split('.')[0]), 'type'] = label
df.to_csv(args.detections, index=False)
