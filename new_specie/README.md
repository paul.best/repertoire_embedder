Scripts to train an auto-encoder and cluster animal vocalisations by frequency-contour similarity.

Vocalisation detection needs to be done prior to this process (stored in a .csv file)



### Scripts need to be called in the following order:

If you want to train your own auto-encoder (optional since the generic one might suffice), use `python train_AE.py detections.csv`

Use trained auto-encoder weights to project your vocalisations with `python compute_embeddings.py generic_embedder.weights detections.csv`
The architecture for the generic_embedder.weights is with a bottleneck of size 256

Visualise vocalisation embeddings and resulting clusters with `python sort_cluster.py embeddings.npy detections.csv`

Use `python myscript.py --help` to get more information on each scripts' usage and options

required packages can be install using  `pip install -r requirements.txt`
