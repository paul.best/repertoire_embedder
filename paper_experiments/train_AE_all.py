from torchvision.utils import make_grid
from torch.utils.tensorboard import SummaryWriter
import torch
import numpy as np, pandas as pd
import utils as u, models
from tqdm import tqdm
import os, argparse, warnings
torch.multiprocessing.set_sharing_strategy('file_system')
warnings.filterwarnings("error")

parser = argparse.ArgumentParser()
parser.add_argument("specie", type=str)
parser.add_argument("-bottleneck", type=int, default=16)
parser.add_argument("-frontend", type=str, default='logMel')
parser.add_argument("-encoder", type=str, default='sparrow_encoder')
parser.add_argument("-nMel", type=int, default=128)
parser.add_argument("-lr", type=float, default=3e-3)
parser.add_argument("-lr_decay", type=float, default=1e-2)
parser.add_argument("-batch_size", type=int, default=128)
parser.add_argument("-cuda", type=int, default=0)
args = parser.parse_args()

species = np.loadtxt('good_species.txt', dtype=str)

modelname = f'ALL_{args.bottleneck}_{args.frontend}{args.nMel if "Mel" in args.frontend else ""}_{args.encoder}_decod2_BN_nomaxPool.stdc'
#modelname = f'ALLbut_{args.specie}_{args.bottleneck}_{args.frontend}{args.nMel if "Mel" in args.frontend else ""}_{args.encoder}_decod2_BN_nomaxPool.stdc'
gpu = torch.device(f'cuda:{args.cuda}')
writer = SummaryWriter(f'runs3/{modelname}')
os.system(f'cp *.py runs3/{modelname}')
vgg16 = models.vgg16
vgg16.eval().to(gpu)


frontends = [models.frontend[args.frontend](models.meta[s]['sr'], models.meta[s]['nfft'], models.meta[s]['sampleDur'], args.nMel).to(gpu) for s in species] # if s != args.specie]
encoder = models.__dict__[args.encoder](args.bottleneck // (args.nMel//32 * 4), (args.nMel//32, 4))
decoder = models.sparrow_decoder(args.bottleneck, (args.nMel//32, 4))
model = torch.nn.Sequential(encoder, decoder).to(gpu)

print('Go for model '+modelname)

optimizer = torch.optim.AdamW(model.parameters(), weight_decay=0, lr=args.lr, betas=(0.8, 0.999))
scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lambda epoch : (1-args.lr_decay)**epoch)
loaders = [torch.utils.data.DataLoader(u.Dataset(pd.read_csv(f'{s}/{s}.csv'), f'{s}/audio/', models.meta[s]['sr'], models.meta[s]['sampleDur']),\
                               batch_size=20, shuffle=True, num_workers=8, prefetch_factor=8, collate_fn=u.collate_fn) for s in species] # if s != args.specie]
iterators = [iter(l) for l in loaders]
MSE = torch.nn.MSELoss()

for step in tqdm(range(15_000)):
    batch = []
    for i in range(len(species)): #-1):
        try:
            x, name = next(iterators[i])
        except (StopIteration):
            iterators[i] = iter(loaders[i])
            x, name = next(iterators[i])
        batch.append(frontends[i](x.to(gpu)))
    label = torch.vstack(batch)
    optimizer.zero_grad()
    x = encoder(label)
    pred = decoder(x)
    assert not torch.isnan(pred).any(), "found a NaN :'("
    predd = vgg16(pred.expand(pred.shape[0], 3, *pred.shape[2:]))
    labell = vgg16(label.expand(label.shape[0], 3, *label.shape[2:]))

    score = MSE(predd, labell)
    score.backward()
    optimizer.step()
    writer.add_scalar('loss', score.item(), step)
    step += 1
    if step % 50 == 0:
        # Plot reconstructions
        images = [(e-e.min())/(e.max()-e.min()) for e in label[:160:20]]
        grid = make_grid(images)
        writer.add_image('target', grid, step)
        # writer.add_embedding(x.detach(), global_step=step, label_img=label)
        images = [(e-e.min())/(e.max()-e.min()) for e in pred[:160:20]]
        grid = make_grid(images)
        writer.add_image('reconstruct', grid, step)

        torch.save(model.state_dict(), f'{args.specie}/weights/{modelname}')
        scheduler.step()
