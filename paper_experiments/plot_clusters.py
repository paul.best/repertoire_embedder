import matplotlib.pyplot as plt
import pandas as pd, numpy as np
import torch, hdbscan
import models, utils as u


species = np.loadtxt('good_species.txt', dtype=str)
fig, ax = plt.subplots(nrows=len(species), figsize=(7, 10), dpi=200)
for i, specie in enumerate(species):
    meta = models.meta[specie]
    frontend = models.frontend['pcenMel'](meta['sr'], meta['nfft'], meta['sampleDur'], 128)
    dic = np.load(f'{specie}/encodings//encodings_{specie}_256_logMel128_sparrow_encoder_decod2_BN_nomaxPool.npy', allow_pickle=True).item()
    idxs, X = dic['idxs'], dic['umap8']
    df = pd.read_csv(f'{specie}/{specie}.csv')
    clusters = hdbscan.HDBSCAN(min_cluster_size=10, min_samples=3, cluster_selection_epsilon=0.1, core_dist_n_jobs=-1, cluster_selection_method='leaf' if not 'humpback' in specie else 'eom').fit_predict(X)
    df.loc[idxs, 'cluster'] = clusters.astype(int)
    for j, cluster in enumerate(np.random.choice(np.arange(df.cluster.max()), 4)):
        loader = torch.utils.data.DataLoader(u.Dataset(df[df.cluster==cluster].sample(8), f'{specie}/audio/', meta['sr'], meta['sampleDur']), batch_size=1)
        for k, (x, name) in enumerate(loader):
            spec = frontend(x).squeeze().numpy()
            ax[i].imshow(spec, extent=[k, k+1, j, j+1], origin='lower', aspect='auto', cmap='Greys', vmin=np.quantile(spec, .2), vmax=np.quantile(spec, .98))
    ax[i].set_xticks([])
    ax[i].set_yticks([])
    if specie == 'humpback2':
        specie = 'humpback\n(small)'
    # ax[i].grid(color='w', xdata=np.arange(1, 10), ydata=np.arange(1, 5))
    ax[i].set_ylabel(specie.replace('_', '\n'))
    ax[i].set_xlim(0, 8)
    ax[i].set_ylim(0, 4)
    ax[i].vlines(np.arange(1, 8), 0, 4, linewidths=1, color='black')
    ax[i].hlines(np.arange(1, 4), 0, 8, linewidths=1, color='black')
plt.subplots_adjust(wspace=0.1)
plt.tight_layout()
plt.savefig('clusters.pdf')