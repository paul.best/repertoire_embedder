from sklearn import metrics
from tqdm import tqdm
import argparse, os
import models, utils as u
import umap
import pandas as pd, numpy as np, torch
from hearbaseline import wav2vec2, torchopenl3, torchcrepe, vggish

parser = argparse.ArgumentParser()
parser.add_argument("specie", type=str)
parser.add_argument("-cuda", type=int, default=0)
args = parser.parse_args()

df = pd.read_csv(f'{args.specie}/{args.specie}.csv')

meta = models.meta[args.specie]
batch_size = 32


gpu = torch.device(f'cuda:{args.cuda}')
for module, name in zip([wav2vec2, torchcrepe, wav2vec2, torchopenl3, vggish], ['wav2vec2', 'crepe', 'openl3', 'vggish']):
    model = module.load_model().to(gpu)
    loader = torch.utils.data.DataLoader(u.Dataset(df, f'{args.specie}/audio/', model.sample_rate, meta['sampleDur']), batch_size=batch_size, num_workers=8, collate_fn=u.collate_fn)
    with torch.inference_mode():
        encodings, idxs = [], []
        for x, idx in tqdm(loader, desc=f'{name} - {args.specie}'):
            encoding = module.get_scene_embeddings(x.to(gpu), model=model)
            idxs.extend(idx.numpy())
            encodings.extend(encoding.view(len(x), -1).cpu().numpy())
    idxs, encodings = np.array(idxs), np.stack(encodings)
    X = umap.UMAP(n_jobs=-1, n_components=8).fit_transform(encodings)
    np.save(f'{args.specie}/encodings/encodings_{name}.npy', {'idxs':idxs, 'encodings':encodings, 'umap8':X})
