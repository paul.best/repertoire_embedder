import matplotlib.pyplot as plt
import pandas as pd, numpy as np

all_frontends = [['256_logMel128', '256_logMel128_noprcptl', 'spec32', 'biosound'][::-1],
                 ['256_logMel128', 'allbut_AE_256_logMel128', 'openl3', 'wav2vec2', 'crepe'][::-1],
                 ['256_logMel128', '256_Mel128', '256_pcenMel128', '256_logSTFT']]

all_frontendNames = [['AE prcptl', 'AE MSE', 'Spectro.', 'PAFs'][::-1],
                     ['AE', 'gen. AE', 'OpenL3', 'Wav2Vec2', 'CREPE'][::-1],
                     ['log-Mel', 'Mel', 'PCEN-Mel', 'log-STFT']]

# Bar plots of NMI depending on the choice of feature extraction
all_plotNames = ['handcraft', 'deepembed', 'frontends']
for frontends, frontendNames, plotName in zip(all_frontends, all_frontendNames, all_plotNames):
    print(plotName)
    df = pd.read_csv('hdbscan_HP.csv')
    df.loc[df.ms.isna(), 'ms'] = 0
    best = df.loc[df.groupby(["specie", 'frontend']).nmi.idxmax()]
    res = [(conf, (grp.set_index(['specie', 'frontend']).nmi / best.set_index(['specie', 'frontend']).nmi).sum()) for conf, grp in df.groupby(['ncomp', 'mcs', 'ms', 'eps', 'al'])]
    conf = res[np.argmax([r[1] for r in res])]
    print('best HP', conf)
    conf = conf[0]

    df = df[((df.ncomp == conf[0])&(df.mcs == conf[1] )&( df.ms==conf[2] )&( df.eps==conf[3] )&(df.al==conf[4] ))]
    df.specie = df.specie.replace('humpback2', 'humpback\n(small)')
    species = df.specie.unique()
    out = pd.DataFrame({fn:[df[((df.specie==s)&(df.frontend==f))].iloc[0].nmi for s in species] for f, fn in zip(frontends, frontendNames)}, index=[s.replace('_','\n') for s in species])
    out.plot.bar(figsize=(9, 3), rot=0)
    plt.legend(bbox_to_anchor=(1,1))
    plt.ylabel('NMI')
    plt.grid(axis='y')
    plt.tight_layout()
    plt.savefig(f'NMIs_hdbscan_barplot_{plotName}.pdf')


# Plot of NMI depending on UMAP nb of components
fig, ax = plt.subplots(ncols=2, sharey=True, sharex=True, figsize=(10, 3.5))
# Left panel with auto-encoder feature extraction
df = pd.read_csv("hdbscan_HP_archive2.csv")
df.loc[df.ms.isna(), "ms"] = 0
df = df[((df.frontend == "256_logMel128")&(df.al=='leaf')&(df.mcs==10)&(df.ms==3)&(df.eps==.1))].sort_values('specie')
#df = df[df.frontend == "256_logMel128"].sort_values('specie')
for s, grp in df.groupby("specie"):
    ax[0].plot(
        np.arange(5),
        [grp[grp.ncomp == n].nmi.max() / grp.nmi.max() for n in 2 ** np.arange(1, 6)],
    )
# Right panel with Spectrogram feature extraction
df = pd.read_csv("hdbscan_HP_archive2.csv")
df.loc[df.ms.isna(), "ms"] = 0
df = df[((df.frontend == "spec32")&(df.al=='leaf')&(df.mcs==10)&(df.ms==3)&(df.eps==.1))].sort_values('specie')
#df = df[df.frontend == "spec32"].sort_values('specie')
for s, grp in df.groupby("specie"):
    if s.endswith('s'):
        s = s[:-1]
    if s == 'humpback2':
        s = 'humpback (small)'
    ax[1].plot(
        np.arange(5),
        [grp[grp.ncomp == n].nmi.max() / grp.nmi.max() for n in 2 ** np.arange(1, 6)],
        label=s.replace('_',' '),
    )
plt.legend()
ax[0].set_ylabel("NMI / max(NMI)")
ax[1].set_ylim(0.85, 1.01)
ax[0].grid()
ax[1].grid()
ax[0].set_title("auto-encoder")
ax[1].set_title("spectrogram")
ax[0].set_xlabel("# UMAP dimensions")
ax[1].set_xlabel("# UMAP dimensions")
ax[0].set_xticks(np.arange(5))
ax[0].set_xticklabels(2 ** np.arange(1, 6))
ax[0].set_yticks(np.arange(0.85, 1.01, 0.05))
plt.tight_layout()
plt.savefig('NMI_umap.pdf')
