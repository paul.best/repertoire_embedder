from torchvision.utils import make_grid
from torch.utils.tensorboard import SummaryWriter
import torch
import numpy as np, pandas as pd
import utils as u, models
from tqdm import tqdm
import os, argparse, warnings
torch.multiprocessing.set_sharing_strategy('file_system')
warnings.filterwarnings("error")

parser = argparse.ArgumentParser()
parser.add_argument("specie", type=str)
parser.add_argument("-bottleneck", type=int, default=256)
parser.add_argument("-frontend", type=str, default='logMel')
parser.add_argument("-encoder", type=str, default='sparrow_encoder')
parser.add_argument("-prcptl", type=int, default=1)
parser.add_argument("-nMel", type=int, default=128)
parser.add_argument("-lr", type=float, default=3e-3)
parser.add_argument("-lr_decay", type=float, default=1e-2)
parser.add_argument("-batch_size", type=int, default=128)
parser.add_argument("-cuda", type=int, default=0)
args = parser.parse_args()

df = pd.read_csv(f'{args.specie}/{args.specie}.csv')
print(f'{len(df)} available vocs')

modelname = f'{args.specie}_{args.bottleneck}_{args.frontend}{args.nMel if "Mel" in args.frontend else ""}_{args.encoder}_decod2_BN_nomaxPool{"_noprcptl" if args.prcptl==0 else ""}.stdc'
gpu = torch.device(f'cuda:{args.cuda}')
writer = SummaryWriter(f'runs/{modelname}')
os.system(f'cp *.py runs/{modelname}')
vgg16 = models.vgg16
vgg16.eval().to(gpu)
meta = models.meta[args.specie]

frontend = models.frontend[args.frontend](meta['sr'], meta['nfft'], meta['sampleDur'], args.nMel)
encoder = models.__dict__[args.encoder](*((args.bottleneck // 16, (4, 4)) if args.nMel == 128 else (args.bottleneck // 8, (2, 4))))
decoder = models.sparrow_decoder(args.bottleneck, (4, 4) if args.nMel == 128 else (2, 4))
model = torch.nn.Sequential(frontend, encoder, decoder).to(gpu)

print('Go for model '+modelname)

optimizer = torch.optim.AdamW(model.parameters(), weight_decay=0, lr=args.lr, betas=(0.8, 0.999))
scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lambda epoch : (1-args.lr_decay)**epoch)
loader = torch.utils.data.DataLoader(u.Dataset(df, f'{args.specie}/audio/', meta['sr'], meta['sampleDur']), \
                               batch_size=args.batch_size, shuffle=True, num_workers=8, prefetch_factor=8, collate_fn=u.collate_fn)
MSE = torch.nn.MSELoss()

step, loss = 0, []
for epoch in range(100_000//len(loader)):
    for x, name in tqdm(loader, desc=str(epoch), leave=False):
        optimizer.zero_grad()
        label = frontend(x.to(gpu))
        assert not torch.isnan(label).any(), "NaN in spectrogram :'( "+str(name[torch.isnan(label).any(1).any(1).any(1)])
        x = encoder(label)
        pred = decoder(x)
        assert not torch.isnan(pred).any(), "found a NaN :'("
        if args.prcptl == 1:
            predd = vgg16(pred.expand(pred.shape[0], 3, *pred.shape[2:]))
            labell = vgg16(label.expand(label.shape[0], 3, *label.shape[2:]))
        else:
            predd, labell = pred, label

        score = MSE(predd, labell)
        score.backward()
        optimizer.step()
        writer.add_scalar('loss', score.item(), step)
        loss.append(score.item())

        if len(loss) > 2000 and np.median(loss[-2000:-1000]) < np.median(loss[-1000:]):
            print('Early stop')
            torch.save(model.state_dict(), f'{args.specie}/weights/{modelname}')
            exit()

        step += 1

        if step % 500 == 0: # scheduler
            scheduler.step()

        # Plot images
        if step % 100 == 0 :
            # Plot reconstructions
            images = [(e-e.min())/(e.max()-e.min()) for e in label[:8]]
            grid = make_grid(images)
            writer.add_image('target', grid, step)
            # writer.add_embedding(x.detach(), global_step=step, label_img=label)
            images = [(e-e.min())/(e.max()-e.min()) for e in pred[:8]]
            grid = make_grid(images)
            writer.add_image('reconstruct', grid, step)