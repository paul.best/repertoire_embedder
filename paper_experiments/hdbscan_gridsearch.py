import hdbscan, umap
import pandas as pd, numpy as np
import matplotlib.pyplot as plt
from sklearn import metrics
import os

species = np.loadtxt('good_species.txt', dtype=str)
frontends = ['spec32','256_logMel128'] #, '256_logSTFT', '256_Mel128', '128_logMel128', '64_pcenMel128', '128_pcenMel128', '256_pcenMel128', '512_pcenMel128']

out = pd.DataFrame(columns=['specie', 'frontend']) if not os.path.isfile('hdbscan_HP.csv') else pd.read_csv('hdbscan_HP.csv')

plt.figure()
for specie in species:
    nmis = []
    for i, frontend in enumerate(frontends):
        df = pd.read_csv(f'{specie}/{specie}.csv')
        fn = f'{specie}/encodings/encodings_' + \
                (f'{specie}_{frontend}_sparrow_encoder_decod2_BN_nomaxPool.npy' if not frontend in ['vggish', 'biosound', 'spec32'] \
                else frontend+'.npy')
        if ((out.frontend==frontend)&(out.specie==specie)).any() or not os.path.isfile(fn):
            continue
        print(specie, frontend)
        dic = np.load(fn, allow_pickle=True).item()
        idxs = dic['idxs']
        df = df.loc[idxs].reset_index()
        for ncomp in [2, 4, 8, 16, 32]:
            X = umap.UMAP(n_jobs=-1, n_components=ncomp).fit_transform(dic['encodings'])
            for mcs in [5, 10, 20, 50, 100, 200]:
                for ms in [None, 3, 5, 10, 20, 30]:
                    for eps in [0.0, 0.01, 0.02, 0.05, 0.1]:
                        for al in ['leaf', 'eom']:
                            if 'indiv' in df.columns:
                                indiv_nmis = []
                                for indiv, grp in df.groupby('indiv'):
                                    clusters = hdbscan.HDBSCAN(min_cluster_size=mcs, min_samples=ms, cluster_selection_epsilon=eps, \
                                        core_dist_n_jobs=-1, cluster_selection_method=al).fit_predict(X[grp.index])
                                    df.loc[grp.index, 'cluster'] = clusters.astype(int)
                                    mask = ~grp.label.isna()
                                    clusters, labels = clusters[mask], grp[mask].label
                                    indiv_nmis.append(metrics.normalized_mutual_info_score(labels, clusters))
                            else:
                                clusters = hdbscan.HDBSCAN(min_cluster_size=mcs, min_samples=ms, cluster_selection_epsilon=eps, \
                                        core_dist_n_jobs=-1, cluster_selection_method=al).fit_predict(X)
                                df['cluster'] = clusters.astype(int)
                                mask = ~df.label.isna()
                                clusters, labels = clusters[mask], df[mask].label
                                tnmi = metrics.normalized_mutual_info_score(labels, clusters)

                            df.drop('cluster', axis=1, inplace=True)
                            out = pd.concat([out, pd.DataFrame([ \
                                {'specie':specie, 'frontend':frontend, 'nmi': np.mean(indiv_nmis) if 'indiv' in df.columns else tnmi, \
                                 'mcs':mcs, 'ms':ms, 'eps':eps, 'al':al, 'ncomp':ncomp}])], ignore_index=True)

        out.to_csv('hdbscan_HP.csv', index=False)
