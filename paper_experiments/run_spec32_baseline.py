from filterbank import STFT, MelFilter, Log1p
import torch, umap
import argparse, tqdm, utils as u
import pandas as pd, numpy as np
import models

parser = argparse.ArgumentParser()
parser.add_argument("specie", type=str)
args = parser.parse_args()

df = pd.read_csv(f'{args.specie}/{args.specie}.csv')

meta = models.meta[args.specie]

loader = torch.utils.data.DataLoader(u.Dataset(df, f'{args.specie}/audio/', meta['sr'], meta['sampleDur']), batch_size=128)

frontend = torch.nn.Sequential(
    STFT(meta['nfft'], int((meta['sampleDur']*meta['sr'] - meta['nfft'])/32) + 1),
    MelFilter(meta['sr'], meta['nfft'], 32, 0, meta['sr']//2),
    Log1p(7, trainable=False),
    torch.nn.InstanceNorm2d(1),
    torch.nn.Flatten()
).to('cuda')

encodings, idxs = [], []
with torch.no_grad():
    for x, idx in tqdm.tqdm(loader):
        encoding = frontend(x.to('cuda'))
        idxs.extend(idx)
        encodings.extend(encoding.cpu().detach())

idxs, encodings = np.array(idxs), np.stack(encodings)
#print(encodings.shape)
#X = umap.UMAP(n_jobs=-1).fit_transform(encodings)
np.save(f'{args.specie}/encodings/encodings_spec32.npy', {'idxs':idxs, 'encodings':encodings}) #, 'umap':X})
