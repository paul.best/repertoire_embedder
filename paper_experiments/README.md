Scripts to run the experiments described in the published paper and plot figures are gathered in this folder.
Specifically :
- train_AE.py to train an auto-encoder for a given dataset
- compute_embeddings.py to use a pretrained auto-encoder to compute vocalisation embeddings
- test_AE.py to cluster given embeddings and compute the NMI with respect to expert labels
- run_XX_baseline.py for baseline feature extraction procedures
- plot_XX.py to plot figures reporting results

use  `python myscript.py --help` to get more information on each scripts' usage

required packages can be install using  `pip install -r requirements.txt`
