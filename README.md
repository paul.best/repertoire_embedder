# Repertoire Embedder

## Description
This repository gathers python scripts to use auto-encoder neural networks on vocalisation spectrograms, allowing to cluster them by frequency-contour similarity.
It was developped to assist bioacousticians in their repertoire discovery procedures, making deep self-supervised learning accessible to non-experts of the field.

For a detailled description of the scientific motivation and experiments corresponding to this repository, please see
Best, P., Marxer, R., Paris, S., & Glotin, H. (2023). Deep audio embeddings for vocalisation clustering. bioRxiv, 2023-03.

## Installation
Necessary python packages can be installed using conda with the environment.yml file, or with pip using requirements.txt files in their corresponding folders.

## Usage
The paper_experiments folder contains scripts and data that were used in the published paper.
Data to reproduce experiments can be found on this [figshare repository](https://doi.org/10.6084/m9.figshare.23138210)

The new_specie folder contains scripts to run auto-encoder embeddings and cluster your own dataset of animal vocalisations. Scripts allow you to train your own auto-encoder, but a generic pretrained encoder is usually suffice (see article).

## License
When using this code in your own experiments, please cite the corresponding paper.